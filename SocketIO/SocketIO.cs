﻿using System;
using System.Net.Sockets;
using System.Text;

public class SocketIO
{
    Socket socket;
    NetworkStream networkStream;
    public SocketIO(TcpClient tcpClient)
    {
        this.socket = tcpClient.Client;
        networkStream = tcpClient.GetStream();
    }

    public void SendString(string str)
    {
        byte[] buffer = Encoding.UTF8.GetBytes(str);
        socket.Send(BitConverter.GetBytes(buffer.Length));
        networkStream.Flush();
        socket.Send(buffer);
        networkStream.Flush();
    }

    public void SendInt32(int value)
    {
        socket.Send(BitConverter.GetBytes(value));
        networkStream.Flush();
    }

    public void SendInt64(long value)
    {
        socket.Send(BitConverter.GetBytes(value));
        networkStream.Flush();
    }
    public void SendByte(byte value)
    {
        byte[] buffer = { value };
        socket.Send(buffer, SocketFlags.None);
        networkStream.Flush();
    }

    public void SendBytes(byte[] buffer, int index, int length)
    {
        socket.Send(buffer, index, length, SocketFlags.None);
        networkStream.Flush();
    }

    public byte ReceiveByte()
    {
        byte[] buffer = new byte[1];
        socket.Receive(buffer);
        return buffer[0];
    }

    public string ReceiveString()
    {
        int stringLength = ReceiveInt32();
        byte[] buffer = new byte[stringLength];
        socket.Receive(buffer);
        string str = Encoding.UTF8.GetString(buffer, 0, stringLength);
        return str;
    }

    public int ReceiveInt32()
    {
        byte[] buffer = new byte[4];
        socket.Receive(buffer);
        return BitConverter.ToInt32(buffer);
    }
    public long ReceiveInt64()
    {
        byte[] buffer = new byte[8];
        socket.Receive(buffer);
        return BitConverter.ToInt64(buffer);
    }
}
