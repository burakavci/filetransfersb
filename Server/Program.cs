﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    class Program
    {
        public const int DEFAULT_PORT = 16666;

        static void Main(string[] args)
        {
            if (!Directory.Exists("./Files"))
            {
                Directory.CreateDirectory("./Files");
            }
            SBServer server = new SBServer();
            server.StartServer(DEFAULT_PORT);
            while (server.Running) ;
        }
    }
}
