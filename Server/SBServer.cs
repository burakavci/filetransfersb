﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class SBServer
    {
        public enum RequestType
        {
            Info = 0x0,
            Data = 0x1
        }

        TcpListener tcp_Server;
        TcpClient remote_Client;
        SocketIO io;
        bool running;

        public void StartServer(int port)
        {
            tcp_Server = new TcpListener(IPAddress.Any, port);
            tcp_Server.Start();
            running = true;
            tcp_Server.BeginAcceptTcpClient((ar) =>
            {
                remote_Client = tcp_Server.EndAcceptTcpClient(ar);
                io = new SocketIO(remote_Client);
                remote_Client.ReceiveBufferSize = remote_Client.SendBufferSize = 1024 * 1024;
                Console.WriteLine("Remote user connected. Connected IP Address {0}", ((IPEndPoint)remote_Client.Client.RemoteEndPoint).Address);
                SerializedFileInfo sfinfo = new SerializedFileInfo();
                sfinfo.Name = "Files";
                sfinfo.Type = SerializedFileInfo.FileType.Directory;
                SendSerializedFileInfo(sfinfo);
                try
                {
                    byte[] requestTypeBuffer = new byte[1];
                    while (true)
                    {
                        remote_Client.Client.Receive(requestTypeBuffer);
                        RequestType requestType = (RequestType)requestTypeBuffer[0];
                        if (requestType == RequestType.Info)
                        {
                            string str = ReceiveRequestPath();
                            str = "./" + str;
                            str = Path.GetFullPath(str);
                            str = Path.GetRelativePath("./", str);
                            Console.WriteLine("User has requested info of \"{0}\"", str);
                            DirectoryInfo dinfo = new DirectoryInfo(str);
                            FileInfo[] files = dinfo.GetFiles();
                            DirectoryInfo[] subDinfos = dinfo.GetDirectories();
                            SendDirectoryInfos(subDinfos);
                            SendFileInfos(files);
                        }
                        else
                        {
                            string str = ReceiveRequestPath();
                            str = "./" + str;
                            str = Path.GetFullPath(str);
                            str = Path.GetRelativePath("./", str);
                            Console.WriteLine("User has requested the \"{0}\"", str);
                            if (Directory.Exists(str))
                            {
                                //requested directory
                                DirectoryInfo dinfo = new DirectoryInfo(str);
                                SendDirectory(dinfo);
                            }
                            else
                            {
                                //requested file
                                FileInfo finfo = new FileInfo(str);
                                SendFile(finfo);
                            }
                        }
                    }
                }
                catch (SocketException ex)
                {
                    Console.WriteLine("Remote user has closed connection.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Something unexpected happened, closing connection.");
                }
                finally
                {
                    running = false;
                    tcp_Server.Stop();
                    tcp_Server.Server.Close();
                    tcp_Server.Server.Dispose();
                }
            }, null);
            Console.WriteLine("Started Listening.");
        }

        string ReceiveRequestPath()
        {
            string str = io.ReceiveString();
            return str;
        }

        public bool Running
        {
            get
            {
                return running;
            }
        }

        void SendDirectoryInfos(DirectoryInfo[] dinfos)
        {
            io.SendInt32(dinfos.Length);
            DirectoryInfo temp_dinfo;
            SerializedFileInfo sfinfo = new SerializedFileInfo();
            for (int i = 0; i < dinfos.Length; i++)
            {
                temp_dinfo = dinfos[i];
                sfinfo.Name = temp_dinfo.Name;
                sfinfo.Type = SerializedFileInfo.FileType.Directory;
                SendSerializedFileInfo(sfinfo);
            }
        }

        void SendDirectory(DirectoryInfo dinfo, string path = "Files\\")
        {
            io.SendString(dinfo.Name);
            DirectoryInfo[] subDirectories = dinfo.GetDirectories();
            FileInfo[] subFiles = dinfo.GetFiles();

            //send sub directory count
            io.SendInt32(subDirectories.Length);
            path += dinfo.Name + '\\';

            Console.WriteLine("Sent directory name of {0}", path);

            for (int i = 0; i < subDirectories.Length; i++)
            {
                SendDirectory(subDirectories[i], path);
            }

            SendFiles(subFiles);
        }



        void SendFiles(FileInfo[] subFiles)
        {
            byte[] buffer = new byte[1024 * 1024 * 100];
            io.SendInt32(subFiles.Length);
            FileInfo finfo;
            long totalSent = 0;
            for (int i = 0; i < subFiles.Length; i++)
            {
                finfo = subFiles[i];
                io.SendInt64(finfo.Length);
                io.SendString(finfo.Name);
                io.SendString(Path.GetRelativePath("./", finfo.FullName));
                double percent = (finfo.Length == 0) ? 100 : (totalSent / (double)finfo.Length) * 100;
                string relativePath = Path.GetRelativePath("./", finfo.FullName);
                Console.WriteLine("Sending {0} ( {1} / {2} bytes ) - {3}%", relativePath, totalSent, finfo.Length, percent.ToString("0.##"));
                double bufferSize = buffer.Length / (1024 * 1024);
                Console.WriteLine("Buffer size: {0} MB", bufferSize.ToString("0.##"));
                //open a stream to read the file
                using (FileStream fs = new FileStream(finfo.FullName, FileMode.Open, FileAccess.Read))
                {
                    int read;
                    int remote_downloaded_size;
                    while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        remote_downloaded_size = 0;
                        io.SendBytes(buffer, 0, read);
                        do
                        {
                            remote_downloaded_size += io.ReceiveInt32();
                        }
                        while (remote_downloaded_size < read);
                        totalSent += read;
                        percent = (totalSent / (double)finfo.Length) * 100;
                        Console.WriteLine("Sending {0} ( {1} / {2} bytes ) - {3}%", relativePath, totalSent, finfo.Length, percent.ToString("0.##"));
                    }
                }
                Console.WriteLine("Sent {0} with success.", relativePath);
            }
        }

        void SendFile(FileInfo finfo)
        {
            byte[] buffer = new byte[1024 * 1024];

            //send file size
            io.SendInt64(finfo.Length);
            long totalSent = 0;
            double percent = (finfo.Length == 0) ? 100 : (totalSent / (double)finfo.Length) * 100;
            string relativePath = Path.GetRelativePath("./", finfo.FullName);
            Console.WriteLine("Sending {0} ( {1} / {2} bytes ) - {3}%", relativePath, totalSent, finfo.Length, percent.ToString("0.##"));
            double bufferSize = buffer.Length / (1024 * 1024);
            Console.WriteLine("Buffer size: {0} MB", bufferSize.ToString("0.##"));
            //open a stream to read the file
            using (FileStream fs = new FileStream(finfo.FullName, FileMode.Open, FileAccess.Read))
            {
                int read;
                int remote_downloaded_size;
                while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                {
                    remote_downloaded_size = 0;
                    io.SendBytes(buffer, 0, read);
                    do
                    {
                        remote_downloaded_size += io.ReceiveInt32();
                    }
                    while (remote_downloaded_size < read);
                    totalSent += read;
                    percent = (totalSent / (double)finfo.Length) * 100;
                    Console.WriteLine("Sending {0} ( {1} / {2} bytes ) - {3}%", relativePath, totalSent, finfo.Length, percent.ToString("0.##"));
                }
            }
            Console.WriteLine("Sent {0} with success.", relativePath);
        }

        void SendFileInfos(FileInfo[] finfos)
        {
            byte[] buffer = BitConverter.GetBytes(finfos.Length);
            remote_Client.Client.Send(buffer);
            FileInfo temp_finfo;
            SerializedFileInfo sfinfo = new SerializedFileInfo();
            for (int i = 0; i < finfos.Length; i++)
            {
                temp_finfo = finfos[i];
                sfinfo.Name = temp_finfo.Name;
                sfinfo.Type = SerializedFileInfo.FileType.File;
                sfinfo.SetIcon(Icon.ExtractAssociatedIcon(temp_finfo.FullName));
                SendSerializedFileInfo(sfinfo);
            }
        }

        void SendSerializedFileInfo(SerializedFileInfo sfinfo)
        {
            byte[] buffer;
            io.SendString(sfinfo.Name);
            byte[] type = new byte[1];
            type[0] = sfinfo.Type == SerializedFileInfo.FileType.Directory ? (byte)0x0 : (byte)0x1;
            remote_Client.Client.Send(type);
            if (sfinfo.Type == SerializedFileInfo.FileType.File)
            {
                buffer = sfinfo.Icon;
                remote_Client.Client.Send(BitConverter.GetBytes(buffer.Length));
                remote_Client.Client.Send(buffer);
            }
        }
    }
}
