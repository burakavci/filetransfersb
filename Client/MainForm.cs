﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileTransferSB
{
    public partial class MainForm : Form
    {
        public static MainForm mainForm;
        public const int DEFAULT_PORT = 16666;

        public enum RequestType
        {
            Info = 0x0,
            Data = 0x1
        }

        TcpClient local_Client;
        SocketIO io;

        public MainForm()
        {
            if (mainForm == null)
            {
                mainForm = this;
                InitializeComponent();
                Debugger.TextBox = debugTextBox;

                if (!Directory.Exists("./Downloads"))
                {
                    Directory.CreateDirectory("./Downloads");
                }
            }
            else
            {
                this.Close();
                return;
            }
        }
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            ConnectButton.Enabled = false;
            IPEndPoint iPEndPoint = IPEndPoint.Parse(TargetEndpointTextBox.Text);
            iPEndPoint.Port = DEFAULT_PORT;
            local_Client = new TcpClient();
            Debugger.WriteLine("Connecting to the server...");
            local_Client.BeginConnect(iPEndPoint.Address, iPEndPoint.Port, (ar) =>
            {
                local_Client.EndConnect(ar);
                io = new SocketIO(local_Client);
                Debugger.WriteLine("Connected to the server.");
                local_Client.ReceiveBufferSize = local_Client.SendBufferSize = 1024 * 1024;
                SerializedFileInfo sfinfo = ReceiveSerializedFileInfo();
                AddTreeViewNode(sfinfo);
            }, null);
        }

        SerializedFileInfo ReceiveSerializedFileInfo()
        {
            SerializedFileInfo sfinfo = new SerializedFileInfo();
            byte[] buffer = new byte[1];
            //receive name
            string str = io.ReceiveString();
            sfinfo.Name = str;
            //second receive file type
            local_Client.Client.Receive(buffer, SocketFlags.None);
            sfinfo.Type = (buffer[0] == 0) ? SerializedFileInfo.FileType.Directory : SerializedFileInfo.FileType.File;
            //third if it's a file, receive icon
            if (sfinfo.Type == SerializedFileInfo.FileType.File)
            {
                int LengthTemp = io.ReceiveInt32();
                Array.Resize<byte>(ref buffer, LengthTemp);
                local_Client.Client.Receive(buffer, SocketFlags.None);
                sfinfo.Icon = buffer;
            }
            return sfinfo;
        }

        TreeNode AddTreeViewNode(SerializedFileInfo sfinfo, TreeNode parent = null)
        {
            return (TreeNode)filesTreeView1.Invoke(new Func<TreeNode>(() =>
            {
                if (parent == null)
                {
                    sfinfo.Icon = null;
                    TreeNode tn = filesTreeView1.Nodes.Add(sfinfo.Name);
                    tn.Nodes.Add("");
                    tn.Tag = sfinfo;
                    return tn;
                }
                else
                {
                    TreeNode tn = parent.Nodes.Add(sfinfo.Name);
                    if (sfinfo.Type == SerializedFileInfo.FileType.File)
                    {
                        imageList1.Images.Add(new Icon(new MemoryStream(sfinfo.Icon)));
                        tn.ImageIndex = tn.SelectedImageIndex = imageList1.Images.Count - 1;
                    }
                    else
                    {
                        tn.Nodes.Add("");
                    }
                    sfinfo.Icon = null;
                    tn.Tag = sfinfo;
                    return tn;
                }
            }));
        }

        void ProgressBarUpdate(double value)
        {
            progressBar1.Invoke(new Action(() =>
            {
                progressBar1.Value = (int)value;
            }));
        }

        void RequestDirectoryInfo(string path)
        {
            io.SendByte((byte)RequestType.Info);
            io.SendString(path);
        }

        void RequestFile(string path)
        {
            io.SendByte((byte)RequestType.Data);
            io.SendString(path);
        }

        void RequestDirectory(string path)
        {
            io.SendByte((byte)RequestType.Data);
            io.SendString(path);
        }

        string GetPathToNode(TreeNode tn)
        {
            string path = "";
            do
            {
                var sfinfo = (SerializedFileInfo)tn.Tag;
                path = sfinfo.Name + '\\' + path;
                tn = tn.Parent;
            } while (tn != null);
            return path.Substring(0, path.Length - 1);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void filesTreeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = (TreeNode)e.Node;
            var sfinfo = (SerializedFileInfo)node.Tag;
            if (sfinfo.Type == SerializedFileInfo.FileType.File)
            {
                string path = GetPathToNode(node);
                RequestFile(path);
                ReceiveFile(path, node.Text);
            }
        }

        void ReceiveDirectoryInfos(TreeNode parent)
        {
            byte[] buffer = new byte[4];
            local_Client.Client.Receive(buffer);
            int directoryLength = BitConverter.ToInt32(buffer);
            for (int i = 0; i < directoryLength; i++)
            {
                SerializedFileInfo sfinfo = ReceiveSerializedFileInfo();
                AddTreeViewNode(sfinfo, parent);
            }
        }
        void ReceiveFileInfos(TreeNode parent)
        {
            byte[] buffer = new byte[4];
            local_Client.Client.Receive(buffer);
            int filesLength = BitConverter.ToInt32(buffer);
            for (int i = 0; i < filesLength; i++)
            {
                SerializedFileInfo sfinfo = ReceiveSerializedFileInfo();
                AddTreeViewNode(sfinfo, parent);
            }
        }

        void ReceiveFile(string remote_file_path, string filePathToSave)
        {
            int bufferSizeMB = Int32.Parse(bufferSizeTextBox.Text);
            bufferSizeMB *= 1024 * 1024;
            byte[] buffer = new byte[bufferSizeMB];
            long fileLength = io.ReceiveInt64();
            string path = "./Downloads/" + filePathToSave;
            path = Path.GetRelativePath("./", path);
            
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                long totalRead = 0;
                int read;
                double percent = (fileLength == 0) ? 100 : 0;
                double bufferSize = buffer.Length / (1024 * 1024);
                ProgressBarUpdate(percent);
                Debugger.WriteLine("Downloading {0} to {4} ( {1} / {2} bytes ) - {3}%", remote_file_path, totalRead, fileLength, percent.ToString("0.##"), path);
                Debugger.WriteLine("Buffer size: {0} MB", bufferSize.ToString("0.##"));
                while (totalRead < fileLength)
                {
                    read = local_Client.Client.Receive(buffer);
                    io.SendInt32(read);
                    fs.Write(buffer, 0, read);
                    totalRead += read;
                    percent = (totalRead / (double)fileLength) * 100;
                    Debugger.WriteLine("Downloading {0} to {4} ( {1} / {2} bytes ) - {3}%", remote_file_path, totalRead, fileLength, percent.ToString("0.##"), path);
                    ProgressBarUpdate(percent);
                }
            }
            Debugger.WriteLine("Downloaded {0} to {1} with success.", remote_file_path, path);
        }

        void ReceiveDirectory(string path = "Downloads\\")
        {
            string directory_name = io.ReceiveString();

            //receive sub directories count
            int subDirectoriesCount = io.ReceiveInt32();

            path += directory_name + '\\';

            Directory.CreateDirectory(path);
            Debugger.WriteLine("Created directory at {0}", path);
            for (int i = 0; i < subDirectoriesCount; i++)
            {
                ReceiveDirectory(path);
            }

            ReceiveFiles(path);
        }

        void ReceiveFiles(string destination_path)
        {
            int bufferSizeMB = Int32.Parse(bufferSizeTextBox.Text);
            bufferSizeMB *= 1024 * 1024;
            byte[] buffer = new byte[bufferSizeMB];
            int subFilesCount = io.ReceiveInt32();

            long fileLength;
            for (int i = 0; i < subFilesCount; i++)
            {
                fileLength = io.ReceiveInt64();

                string file_name = io.ReceiveString();
                string localFilePath = destination_path + file_name;

                string remoteFilePath = io.ReceiveString();
                using (FileStream fs = new FileStream(localFilePath, FileMode.Create))
                {
                    Array.Resize<byte>(ref buffer, 1024 * 1024);
                    Array.Clear(buffer, 0, buffer.Length);
                    long totalRead = 0;
                    int read;
                    double percent = (fileLength == 0) ? 100 : 0;
                    double bufferSize = buffer.Length / (1024 * 1024);
                    ProgressBarUpdate(percent);

                    Debugger.WriteLine("Downloading {0} to {4} ( {1} / {2} bytes ) - {3}%", remoteFilePath, totalRead, fileLength, percent.ToString("0.##"), localFilePath);
                    Debugger.WriteLine("Buffer size: {0} MB", bufferSize.ToString("0.##"));
                    while (totalRead < fileLength)
                    {
                        read = local_Client.Client.Receive(buffer);
                        io.SendInt32(read);
                        fs.Write(buffer, 0, read);
                        totalRead += read;
                        percent = (totalRead / (double)fileLength) * 100;
                        Debugger.WriteLine("Downloading {0} to {4} ( {1} / {2} bytes ) - {3}%", remoteFilePath, totalRead, fileLength, percent.ToString("0.##"), localFilePath);
                        ProgressBarUpdate(percent);
                    }
                }
                Debugger.WriteLine("Downloaded {0} to {1} with success.", remoteFilePath, localFilePath);
            }
        }

        private void filesTreeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            //TODO: demand files and folders from remote server bla bla bla
            TreeNode node = (TreeNode)e.Node;
            if (node.Nodes.Count > 0 && node.Nodes[0].Tag == null)
            {
                node.Nodes.Clear();
                string path = GetPathToNode(node);
                RequestDirectoryInfo(path);
                ReceiveDirectoryInfos(node);
                ReceiveFileInfos(node);
                //RemoveEmptyDirectoryNode(node);
            }
        }

        void RemoveEmptyDirectoryNode(TreeNode node)
        {
            filesTreeView1.Invoke(new Action(() =>
            {
                node.Nodes.RemoveAt(0);
            }));
        }

        private void refreshMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode node = filesTreeView1.SelectedNode;
            if (node != null)
            {
                var sfinfo = (SerializedFileInfo)node.Tag;
                if (sfinfo.Type == SerializedFileInfo.FileType.Directory)
                {
                    node.Nodes.Clear();
                    string path = GetPathToNode(node);
                    RequestDirectoryInfo(path);
                    ReceiveDirectoryInfos(node);
                    ReceiveFileInfos(node);
                    //if (node.Nodes.Count > 0 && node.Nodes[0].Tag == null)
                    //    RemoveEmptyDirectoryNode(node);
                }
            }
        }

        private void downloadMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode node = filesTreeView1.SelectedNode;
            var sfinfo = (SerializedFileInfo)node.Tag;
            if (sfinfo.Type == SerializedFileInfo.FileType.Directory)
            {
                string path = GetPathToNode(node);
                RequestDirectory(path);
                ReceiveDirectory();
                Debugger.WriteLine("Downloaded {0} with success.", path);
            }
            else
            {
                string path = GetPathToNode(node);
                RequestFile(path);
                ReceiveFile(path, node.Text);
            }
        }

        private void filesTreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            filesTreeView1.SelectedNode = e.Node;
        }
    }
}
